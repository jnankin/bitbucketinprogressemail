#!/usr/bin/python 
import requests, string, random, sys
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib
from optparse import OptionParser
import yaml
from feed.date import rfc3339
import datetime
from dateutil.parser import parse
import time
from xml.etree import ElementTree
import os

def formatPriority(priority):
    return {
	'minor' : 'minor',
        'major' : '<strong>major</strong>',
        'critical' : '<strong><font color="red">critical</font></strong>',
        'blocker' : '<strong><font color="red">BLOCKER</font></strong>',
        'trivial' : priority,
    }[priority]

"""
	A script to collect all in progress tasks in bitbucket to be emailed to the crew daily
"""

bitbucketApiEndpoint = 'https://bitbucket.org/api/1.0/'
confFile = os.path.dirname(os.path.realpath(__file__)) + "/config.yml"

parser = OptionParser()
parser.add_option("--configfile", help="Location of configuration file", default=confFile)
(options, args) = parser.parse_args()

stream = open(options.configfile, 'r')
conf = yaml.load(stream)

if not 'email' in conf or not 'user' in conf or not 'pass' in conf:
    print "missing required parameters"
    sys.exit(1)

authTuple = (conf['user'], conf['pass'])
team = conf['team'] if 'team' in conf else conf['user']

# get all repos for the team
repos = requests.get(bitbucketApiEndpoint + 'users/' + team, auth=authTuple).json()
repos = repos['repositories']

userProjectTasks = {}

# for each repo, get all inprogress items
for repo in repos:
    if not repo['has_issues']: continue

    # add each inprogress item to a hashmap of user=>project=>tasks
    inprogressItems = requests.get(
        bitbucketApiEndpoint + 'repositories/{0}/{1}/issues/?status=open&status=new&sort=priority&limit=50'.format(team,
                                                                                                                   repo[
                                                                                                                       'slug']),
        auth=authTuple).json()
    if (inprogressItems['count']):
        for issue in inprogressItems['issues']:
            if not 'responsible' in issue: continue

            if not issue['responsible']['username'] in userProjectTasks:
                userProjectTasks[issue['responsible']['username']] = {}

            if not repo['slug'] in userProjectTasks[issue['responsible']['username']]:
                userProjectTasks[issue['responsible']['username']][repo['slug']] = []

            userProjectTasks[issue['responsible']['username']][repo['slug']].insert(0, {
                'id': str(issue['local_id']),
                'title': issue['title'],
                'priority': issue['priority']
            })

# format the email and send it
body = ""
for user in userProjectTasks:
    body += "<h1>" + user + "</h1>"

    if 'calendars' in conf and user in conf['calendars']:
        body += "<h2>Today's Workload</h2><ul>"
        todayTimestamp = (parse(time.strftime("%Y-%m-%d") + " 00:00:00") - datetime.datetime(1970,1,1)).total_seconds()
        today = rfc3339.timestamp_from_tf(todayTimestamp)
        tomorrow = rfc3339.timestamp_from_tf(todayTimestamp + 86400);

        data = requests.get(
            conf['calendars'][user],
            params={'start-min': today, 'start-max': tomorrow}).text
        root = ElementTree.fromstring(data.encode('utf-8'))
        entries = root.findall(".//{http://www.w3.org/2005/Atom}entry")
        for entry in entries:
            body += "<li>" + entry.findtext('{http://www.w3.org/2005/Atom}title') + "</li>"
        body += "</ul>"


    body += "<h2>Assigned issues</h2>"

    for repo in userProjectTasks[user]:
        body += "<h3>" + repo + "<h3><ul>"
        for issue in userProjectTasks[user][repo]:
            body += "<li><a href='http://bitbucket.org/" + team + "/" + repo + "/issue/" + issue['id'] + "'>" + \
                    formatPriority(issue['priority']) + " #" + issue['id'] + " " + issue['title'] + "</a></li>"
        body += "</ul>"
    body += "<br><br>"

msg = MIMEMultipart('alternative')
msg['Subject'] = "Daily bitbucket inprogress tickets for " + team
msg['To'] = conf['email']
msg['From'] = conf['email']
msg.attach(MIMEText(body, 'html'))

smtpconf = conf['smtp']
mail = smtplib.SMTP(smtpconf['host'], smtpconf['port'])
mail.starttls()
mail.login(smtpconf['user'], smtpconf['pass'])
mail.sendmail(conf['email'], conf['email'], msg.as_string())
mail.quit()
